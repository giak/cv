const Handlebars = require("handlebars");
const {
  library,
  dom,
  icon,
  findIconDefinition,
} = require("@fortawesome/fontawesome-svg-core");
const { fas } = require("@fortawesome/free-solid-svg-icons");
const { fab } = require("@fortawesome/free-brands-svg-icons");

const ICON_PREFIX = "fas";
const ICON_PREFIX_BRANDS = "fab";
const ICON_SIZE = "fa-2xs";

library.add(fas, fab);

Handlebars.registerHelper(
  "fontawesome-css",
  () => new Handlebars.SafeString(dom.css())
);

const specialIcons = {
  telegram: ICON_PREFIX_BRANDS,
  "linkedin-square": ICON_PREFIX_BRANDS,
  "github-square": ICON_PREFIX_BRANDS,
  "x-twitter": ICON_PREFIX_BRANDS,
};

Handlebars.registerHelper("fontawesome-icon", (args) => {
  let iconName = args.hash.icon;

  if (!iconName) {
    return;
  }

  const prefix = specialIcons[iconName] || ICON_PREFIX;
  if (iconName === "linkedin-square") {
    iconName = "linkedin";
  }

  const iconDefinition = findIconDefinition({
    prefix: prefix,
    iconName: iconName,
  });

  if (!iconDefinition) {
    return;
  }
  const iconHtml = icon(iconDefinition, { classes: [ICON_SIZE] }).html;
  return new Handlebars.SafeString(iconHtml[0]);
});
