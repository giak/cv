require("dotenv").config();
const fs = require("fs");
const handlebars = require("handlebars");
const handlebarsWax = require("handlebars-wax");
const addressFormat = require("address-format");
const moment = require("moment");
const Swag = require("swag");
const minify = require("html-minifier").minify;
require("./helper.js");
import {
  LOCALE,
  DATE_FORMAT,
  DATE_SHORT_FORMAT,
  TEXT_ENCODE,
  getResumeFileAndThemeFolder,
} from "./config.js";

Swag.registerHelpers(handlebars);

const { resumeFile, themeFolder, resumeFileHtml } = getResumeFileAndThemeFolder(
  process.env.RESUME_CHOICE
);
const RESUME_PATH_FILE = `${__dirname}/../${resumeFile}`;
const THEME_FOLDER = `${__dirname}/${themeFolder}`;
const RESUME_PATH = `${THEME_FOLDER}/views/resume.hbs`;
const PARTIALS_PATH = `${THEME_FOLDER}/views/partials/**/*.{hbs,js}`;
const COMPONENTS_PATH = `${THEME_FOLDER}/views/components/**/*.{hbs,js}`;
const THEME_CSS = `${THEME_FOLDER}/styles/main.css`;

const removeProtocol = (url) => url.replace(/.*?:\/\//g, "");
const formatDate = (date) => moment(date).locale(LOCALE).format(DATE_FORMAT);
const shortFormatDate = (date) =>
  moment(date).locale(LOCALE).format(DATE_SHORT_FORMAT);
const formatAddress = (address, city, region, postalCode, countryCode) =>
  addressFormat({
    address,
    city,
    subdivision: region,
    postalCode,
    countryCode,
  }).join("<br>");
const concat = (...args) =>
  args.filter((arg) => typeof arg !== "object").join("");
const formatString = (text) =>
  text
    ?.split("|")
    .map((item) => `<span>${item}</span>`)
    .join("");

handlebars.registerHelper({
  removeProtocol,
  formatDate,
  shortFormatDate,
  formatAddress,
  concat,
  formatString,
});

const render = () => {
  const resumeFileContent = JSON.parse(
    fs.readFileSync(RESUME_PATH_FILE, TEXT_ENCODE)
  );
  const themeCss = fs.readFileSync(THEME_CSS, TEXT_ENCODE);
  const resumeTemplate = fs.readFileSync(RESUME_PATH, TEXT_ENCODE);

  const Handlebars = handlebarsWax(handlebars);
  Handlebars.partials(PARTIALS_PATH);
  Handlebars.partials(COMPONENTS_PATH);

  const html = Handlebars.compile(resumeTemplate)({
    css: themeCss,
    resume: resumeFileContent,
  });

  return minify(html, {
    collapseWhitespace: true,
    removeTagWhitespace: true,
    minifyCSS: true,
    removeComments: true,
    removeRedundantAttributes: true,
  });
};

try {
  fs.writeFileSync(resumeFileHtml, render());
  console.log(`The file ${resumeFileHtml} was saved!`);
} catch (err) {
  console.error(err);
}

export { render };
