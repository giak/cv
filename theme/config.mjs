import dotenv from "dotenv";
dotenv.config();

export const LOCALE = "fr";
export const DATE_FORMAT = "MMMM YYYY";
export const DATE_SHORT_FORMAT = "MM/YYYY";
export const TEXT_ENCODE = "utf-8";
const defaultTheme = process.env.RESUME_THEME_LIGHT_FOLDER;

export const resumeThemes = {
  light: {
    resumeFile: process.env.RESUME_FILE_LIGHT,
    themeFolder: process.env.RESUME_THEME_LIGHT_FOLDER,
    resumeFileHtml: process.env.RESUME_FILE_HTML_LIGHT,
    resumeFilePdf: process.env.RESUME_FILE_PDF_LIGHT,
  },
  public: {
    resumeFile: process.env.RESUME_FILE,
    themeFolder: process.env.RESUME_THEME_FOLDER,
    resumeFileHtml: process.env.RESUME_FILE_HTML,
    resumeFilePdf: process.env.RESUME_FILE_PDF,
  },
};

export function getResumeFileAndThemeFolder(themeChoice = defaultTheme) {
  if (!Object.prototype.hasOwnProperty.call(resumeThemes, themeChoice)) {
    console.warn(
      `Invalid theme choice "${themeChoice}", defaulting to 'classic'.`
    );
  }

  const { resumeFile, themeFolder, resumeFileHtml, resumeFilePdf } =
    resumeThemes[themeChoice];

  return { resumeFile, themeFolder, resumeFileHtml, resumeFilePdf };
}
