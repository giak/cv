import dotenv from 'dotenv';
import fs from 'fs';
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import { TEXT_ENCODE, getResumeFileAndThemeFolder } from './config.mjs';
dotenv.config();
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const { resumeFile, themeFolder, resumeFileHtml, resumeFilePdf } = getResumeFileAndThemeFolder(
  process.env.RESUME_CHOICE,
);
const RESUME_PATH_FILE = `${__dirname}/../${resumeFileHtml}`;

const resumeFileContent = fs.readFileSync(RESUME_PATH_FILE, TEXT_ENCODE);

import puppeteer from 'puppeteer';

const browser = await puppeteer.launch({
  headless: 'new',
  args: [
    '--no-sandbox',
    '--disable-setuid-sandbox',
    '--disable-dev-shm-usage',
    '--disable-gpu',
    '--window-size=1920x1080',
    '--font-render-hinting=none'
  ],
  protocolTimeout: 240000,
  defaultViewport: {
    width: 1920,
    height: 1080,
    deviceScaleFactor: 2
  }
});
const page = await browser.newPage();

await page.setContent(resumeFileContent, {
  waitUntil: ['networkidle0', 'domcontentloaded'],
  timeout: 240000
});

// Set viewport and emulate print media
await page.setViewport({ width: 595, height: 842, deviceScaleFactor: 1 }); // A4 dimensions in pixels
await page.emulateMediaType('print');

await page.pdf({
  path: resumeFilePdf,
  format: 'a4',
  printBackground: true,
  margin: {
    top: '10mm',
    right: '10mm',
    bottom: '10mm',
    left: '10mm'
  },
  scale: 0.85,
  preferCSSPageSize: false,
  displayHeaderFooter: false
});
await browser.close();
